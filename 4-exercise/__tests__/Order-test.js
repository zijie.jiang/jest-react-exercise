import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import axiosMock from 'axios';
import 'babel-polyfill';

import OrderComponent from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  // setup组件
  const { getByLabelText, getByTestId } = render(<OrderComponent />);
  const input = getByLabelText('number-input');
  const button = getByLabelText('submit-button');
  // Mock数据请求
  axiosMock.get.mockResolvedValue({ data: { status: '已完成' } });
  // 触发事件
  fireEvent.change(input, { target: { value: '1' } });
  await fireEvent.click(button);
  // 给出断言
  const p = getByTestId('status');
  expect(p).toHaveTextContent('已完成');
  // --end->
});
